package my_lists_page;



import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import core_page.CorePage;
import core_page.Direction;
import io.qameta.allure.Step;

import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class MyListsPage extends CorePage {

    private final static ElementsCollection
            ARTICLES_TITLE = $$("[resource-id = 'org.wikipedia:id/page_list_item_title']");

    /**
     * Поиск элемента по тексту
     * @param substring текст искомого элемента
     * @return Selenide Element
     */
    private static SelenideElement findAnElementArticleByText(String substring){
        return $x("//*[@text = '" + substring + "']");
    }
    @Step("Кликнуть по элементу с текстом {substring}")
    public static void clickOnAnElement(String substring){
        findAnElementArticleByText(substring)
                .click();
    }

    @Step("Проверка названия и видимости статьи {nameArticle}")
    public static void checkArticle(String nameArticle){
        findAnElementArticleByText(nameArticle)
                .should(visible)
                .should(text(nameArticle));
    }

    @Step("Проверка отсутствия статьи {nameArticle}")
    public static void checkForMissingArticle(String nameArticle){
        findAnElementArticleByText(nameArticle)
                .shouldNot(exist);
    }

    @Step("Проверка присутствия статьи {nameArticle}")
    public static void checkThePresenceOfTheArticle(String nameArticle){
        findAnElementArticleByText(nameArticle)
                .should(exist);
    }

    @Step("Удалить статью {nameArticle}")
    public static void deletedArticle(String nameArticle){
        makeSwipeElement(findAnElementArticleByText(nameArticle), Direction.RIGHT, 3000);
    }

    @Step("Проверить, что количество статей равно {number}")
    public static void checkTheNumberOfArticles(int number){
        ARTICLES_TITLE.should(size(number));
    }
}
