package search_page;
import core_page.CorePage;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.appium.SelenideAppium.$;
import static com.codeborne.selenide.appium.SelenideAppium.$x;
import static java.time.Duration.*;
public class SearchPage extends CorePage {

    private static final SelenideElement
            SEARCH_INIT_ELEMENT = $x("//*[contains(@text, 'Search Wikipedia')]"),
            SEARCH_INPUT = $(".android.widget.EditText"),
            RESULT_SEARCH_JAVA_OOP = $x("//*[@text = 'Object-oriented programming language']"),
            CLOSE_BTN_X = $("[resource-id = 'org.wikipedia:id/search_close_btn']"),
            NO_RESULTS = $("[resource-id = 'org.wikipedia:id/results_text']");

    private static final ElementsCollection
            ARTICLES_TITLE = $$("[resource-id = 'org.wikipedia:id/page_list_item_title']");

    private static final String
            SEARCH_TEXT = "Java",
            RESULT_SEARCH = "Object-oriented programming language";

    /**
     * Поиск элемента по тексту
     * @param substring текст элемента
     * @return Selenide Element
     */
    private static SelenideElement findAnElementArticleByText(String substring){
        return $x("//*[@text = '" + substring + "']");
    }

    /**
     * "До статьи"
     * @param nameArticle название статьи
     * @return Selenide Element
     */
    public static SelenideElement toArticle(String nameArticle){
        return findAnElementArticleByText(nameArticle);
    }

    @Step("Нажать на поисковую строку")
    public static void initSearchInput(){
        SEARCH_INIT_ELEMENT.click();
    }

    @Step("Вводим {input} в поисковую строку")
    public static void setSearchInput(String input){
        SEARCH_INPUT.setValue(input);
    }

    @Step("Проверка видимости результатов поиска")
    public static void visibleResultSearch(String substring){
        findAnElementArticleByText(substring)
                .should(visible,
                        ofSeconds(10));
    }

    @Step("Нажать на статью {nameArticle}")
    public static void clickArticle(String nameArticle){
        findAnElementArticleByText(nameArticle).click();
    }

    @Step("Отменить поиск")
    public static void clickCloseBtnX(){
        CLOSE_BTN_X.click();
    }

    @Step("Проверка видимости кнопки удаления запроса из поисково строки")
    public static void checkButtonXVisibility(){
        CLOSE_BTN_X.shouldNot(exist);
    }

    @Step("Проверка текста в поисковой строке ")
    public static void checkTextInTheSearchBar(String text){
        SEARCH_INPUT.should(exactText(text));
    }

    @Step("Проверка результатов поиска")
    public static void checkNotEmptySearch(){
        ARTICLES_TITLE.should(sizeGreaterThan(0));
    }

    @Step("Проверка отсутствия результатов поиска")
    public static void checkEmptySearch(){
        ARTICLES_TITLE.should(size(0));
    }

    @Step("Проверка лейбла 'No Result' при отсутствии результатов поиска ")
    public static void checkTextNoResult(){
        NO_RESULTS.should(exist);
    }

    @Step("Проверка слова {textArticles} в найденных статьях")
    public static void checkTextInFoundArticles(String textArticles){
        checkTexts(ARTICLES_TITLE, textArticles );
    }

    @Step("Проверка количества статей")
    public static void checkNumbersArticles(int numbersArticles){
        ARTICLES_TITLE.should(size(numbersArticles));
    }

    @Step("Проверяем , что статья с именем {searchResult} есть в найденных статьях ")
    public static void checkSearchResult(String searchResult){
        findAnElementArticleByText(searchResult).should(exist);
    }
}
