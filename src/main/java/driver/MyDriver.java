package driver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ScreenOrientation;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import static driver.DriverConstant.*;

public class MyDriver {

    private static AppiumDriver driver;

    public static AppiumDriver setDriver() throws MalformedURLException{
        URL URL = new URL(APPIUM_URL);
        if(true){                                                  //Требуется расширить метод для разных платформ
            return driver = new AndroidDriver(URL, getAndroidCapabilities());
        }else {
            throw new IllegalArgumentException("Cannot detect type of the Driver");
        }
    }


    private static Capabilities getAndroidCapabilities(){
        UiAutomator2Options options = new UiAutomator2Options();
        options.setPlatformName(PLATFORM);
        options.setPlatformVersion(VERSION);
        options.setDeviceName(DEVICE_NAME);
        options.setNewCommandTimeout(Duration.ofSeconds(TIME_OUT));
        options.setApp(APK_PATH);
        options.setAppPackage(APK_PACKAGE);
        options.setAppActivity(MAIN_ACTIVITY);
        options.setAutomationName(AUTOMATION_NAME);
        options.setFullReset(false);
        options.setOrientation(ScreenOrientation.PORTRAIT);
        options.setCapability("unicodeKeyboard", true);
        options.setCapability("resetKeyboard", true);
        return options;
    }

    public static AppiumDriver getDriver(){
        return driver;
    }

}
