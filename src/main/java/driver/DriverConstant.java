package driver;

public class DriverConstant {

    protected final static int
            TIME_OUT = 10;
    protected final static String
            PLATFORM = "Android",
            VERSION = "11.0",
            DEVICE_NAME = "Redmi Note 7",
            APK_PATH = "E:/IT/Project/JavaAppiumAutomation/apks/org.wikipedia_2.7.50431-r-2023-02-22_50431.apk",
            APPIUM_URL = "http://127.0.0.1:4723/wd/hub",
            APK_PACKAGE = "org.wikipedia",
            MAIN_ACTIVITY = "org.wikipedia.main.MainActivity",
            AUTOMATION_NAME = "Appium";

}
