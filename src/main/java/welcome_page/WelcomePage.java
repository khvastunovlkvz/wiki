package welcome_page;

import com.codeborne.selenide.appium.SelenideAppiumElement;
import core_page.CorePage;
import io.qameta.allure.Step;


import static com.codeborne.selenide.appium.SelenideAppium.$x;

public  class WelcomePage extends CorePage {


    private static final SelenideAppiumElement SKIP_BUTTON = $x("//*[contains(@text,'SKIP')]");

    @Step("Пропустить страницу WelcomePage")
    public static void skipWelcomePage(){
        SKIP_BUTTON.click();
    }
}
