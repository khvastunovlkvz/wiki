package listener;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestContext;
import org.testng.ITestResult;

public class MyScreenshotClass implements IInvokedMethodListener {

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult, ITestContext context) {
        if (!testResult.isSuccess()){
           screenshot();
        }
    }
    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] screenshot(){
        return Selenide.screenshot(OutputType.BYTES);
    }



}
