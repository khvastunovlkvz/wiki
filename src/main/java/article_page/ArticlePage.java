package article_page;

import core_page.CorePage;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.*;

import java.time.Duration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.appium.SelenideAppium.$;
import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class ArticlePage extends CorePage {

    private final static SelenideElement
            PAGE_SAVE = $("[resource-id = 'org.wikipedia:id/page_save']"),
            ADD_TO_READING_LIST = $x("//*[@text = 'ADD TO LIST']"),
            TEXT_INPUT = $("[resource-id = 'org.wikipedia:id/text_input']"),
            BUTTON_OK = $("[resource-id = 'android:id/button1']"),
            ITEM_TITLE_LIST = $("[resource-id = 'org.wikipedia:id/item_title']"),
            DESCRIPTION_ARTICLE = $x("//*[@resource-id = 'pcs-edit-section-title-description']");

    private static SelenideElement findAnElementArticleByText(String text){
        return $x("//*[@text = '" + text + "']");
    }

    @Step("Проверка описания статьи")
    public static void checkDescriptionArticle(String elementArticleText, int time){
       findAnElementArticleByText(elementArticleText)
               .should(text(elementArticleText), Duration.ofSeconds(time));
    }

    @Step("Добавить статью в новый список {nameOfThisList}")
    public static void AddAnArticleToANewList(String nameOfThisList){
        PAGE_SAVE
                .should(visible, Duration.ofSeconds(5))
                .click();
        ADD_TO_READING_LIST
                .should(visible, Duration.ofSeconds(5))
                .click();
        TEXT_INPUT
                .should(visible)
                .setValue(nameOfThisList);
        BUTTON_OK
                .should(visible)
                .click();

    }
    @Step("Добавить статью в мой список {nameOfThisList}")
    public static void AddAnArticleToMyList(String nameOfThisList){
        PAGE_SAVE
                .should(visible, Duration.ofSeconds(5))
                .click();
        ADD_TO_READING_LIST
                .should(visible, Duration.ofSeconds(5))
                .click();
        findAnElementArticleByText(nameOfThisList)
                .click();
    }
    @Step("Получить текст описания статьи")
    public static String getTextDescriptionArticle(){
        return DESCRIPTION_ARTICLE.getText();
    }

}
