package core_page;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.ImmutableMap;
import driver.MyDriver;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import java.time.Duration;
import static com.codeborne.selenide.Condition.*;
public class CorePage {
    /**
     * Метод сравнивает текст в коллекциях с ожидаемым текстом
     * @param collection коллекция Selenide Element
     * @param expected ожидаемый текст
     */
    public static void checkTexts(ElementsCollection collection, String expected){
        for(SelenideElement element : collection){
            element.should(text(expected));
        }
    }
    //________________________________________________________________________________________________________________//

    /**
     * Метод выбирает направление
     * @param dir выбор направления
     * @return возвращает направление в виде строки
     */
    protected static String selectOfDirection(Direction dir){
        String direction;
        //--------------------------//
        switch (dir){                                                     // dir - указываем направление свайпа;
            case UP -> direction = "up";
            case DOWN -> direction ="down";
            case LEFT -> direction ="left";
            case RIGHT -> direction ="right";
            default -> throw new IllegalArgumentException("swipeScreen(): dir: '" + dir + "' NOT supported");
        }
        return direction;
    }
    //________________________________________________________________________________________________________________//

    /**
     * Метод свайп
     * @param dir направление свайпа
     * @param speed скорость свайпа
     * @param percentage процент свайпа
     * @param startOffset ограничение рабочего окна с 4-х сторон на величину startOffset (Максимальное значение 45)
     */
    public static void makeSwipe(Direction dir, int speed, int percentage, int startOffset ){

        Dimension dims =
                MyDriver
                .getDriver()
                .manage()
                .window()
                .getSize();                                         //Получаем размер экрана;
        //--------------------------//
        double offset;
        if(startOffset <= 45){
            offset = startOffset / 100.0;                           //Доработать. Старт свайпа.
            } else {
                throw new IllegalArgumentException("swipeScreen(): startOffset: '" + startOffset + "' > 45");
            }
        //--------------------------//
        double percent = percentage / 100.0;                          //Процент свайпа;
        //--------------------------//
        int edgeBorderLeft = (int)(dims.width * offset);              //Размер границы слева;
        int edgeBorderTop = (int)(dims.height * offset);              //Размер границы сверху;
        int edgeWidth = (int)(dims.width - (2.0 * edgeBorderLeft));   //Размер границы справа;
        int edgeHeight = (int)(dims.height - (2.0 * edgeBorderTop));  //Размер границы снизу;
        //--------------------------//
        ((JavascriptExecutor) MyDriver.getDriver())
                .executeScript("mobile: swipeGesture", ImmutableMap.of(
                "left", edgeBorderLeft,                                     // Ограничение слева
                "top", edgeBorderTop,                                           // Ограничение сверху
                "width", edgeWidth ,                                            // Ширина рабочей области
                "height", edgeHeight ,                                          // Высота рабочей области
                "direction", selectOfDirection(dir),                            // Направление свайпа
                "percent", percent,                                             // Процент свайпа
                "speed", speed));
        //--------------------------//
        try {                                                                    //Режим ожидания;
            Thread.sleep(1500);
        } catch (InterruptedException ignored){}
    }
    //________________________________________________________________________________________________________________//
    /**
     * Метод свайп
     * @param dir направление свайпа
     */
    public static void makeSwipe(Direction dir){
        makeSwipe(dir, 2000, 100, 10);
    }
    //________________________________________________________________________________________________________________//
    /**
     * Метод свайп
     * @param dir направление свайпа
     * @param speed скорость свайпа
     */
    public static void makeSwipe(Direction dir, int speed){
        makeSwipe(dir, speed, 100, 10);
    }
    //________________________________________________________________________________________________________________//
    /**
     * Метод свайп
     * @param dir направление свайпа
     * @param speed скорость свайпа
     * @param percentage процент свайпа
     */
    public static void makeSwipe(Direction dir, int speed, int percentage){
        makeSwipe(dir, speed, percentage, 10);
    }
    //________________________________________________________________________________________________________________//

    /**
     * Свайп до элемента
     * @param element Selenide Element
     * @param maxSwipes максимальное количество свайпов
     */
    public static void swipeUpToFindElement(SelenideElement element, int maxSwipes){

        int allReadySwipes = 0;

        while (!element.isDisplayed()){
            if(allReadySwipes >= maxSwipes){
                throw  new IllegalArgumentException("swipeUpToFindElement: '" + element + "' not find") ;
            }
        makeSwipe(Direction.UP);
        ++allReadySwipes;
        }
    }
    //________________________________________________________________________________________________________________//

    /**
     * Свайп до элемента
     * @param element Selenide Element
     */
    public static void swipeUpToFindElement(SelenideElement element){
        swipeUpToFindElement(element,10);
    }
    //________________________________________________________________________________________________________________//

    /**
     * Свайп по элементу
     * @param element Selenide element
     * @param dir направление свайпа
     * @param speed скорость свайпа
     */
    public static void makeSwipeElement(SelenideElement element, Direction dir, int speed){
       ((JavascriptExecutor)
                MyDriver.getDriver())
                .executeScript("mobile: swipeGesture", ImmutableMap.of(
                        "elementId",element,
                        "direction", selectOfDirection(dir),                         // Направление свайпа
                        "percent", 0.75,                                             // Процент свайпа
                        "speed", speed));
        //--------------------------//
        try {                                                                    //Режим ожидания;
            Thread.sleep(1500);
        } catch (InterruptedException ignored){}
    }
    //________________________________________________________________________________________________________________//

    /**
     * Свайп по элементу
     * @param element Selenide element
     * @param dir направление свайпа
     */
    public static void makeSwipeElement(SelenideElement element, Direction dir){
        makeSwipeElement(element, dir, 2000);
    }
    @Step("Выбрать ориентацию {orientation}")
    public static void selectOrientation(ScreenOrientation orientation){
        ((AndroidDriver) MyDriver.getDriver()).rotate(orientation);
    }
    //________________________________________________________________________________________________________________//
    @Step("Свернуть приложение на {timeSeconds} секунд ")
    public static void appInBackground(int timeSeconds){
        ((AndroidDriver) MyDriver.getDriver()).runAppInBackground(Duration.ofSeconds(timeSeconds));
    }
}
