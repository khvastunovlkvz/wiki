package navigation_ui;

import core_page.CorePage;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.appium.SelenideAppium.$;

public class NavigationUI extends CorePage {

    private final static SelenideElement
            RETURN_BACK = $("[content-desc = 'Navigate up']"),
            SAVED = $("[content-desc = 'Saved']");
    @Step("Нажать на кнопку назад")
    public static void back(){
        RETURN_BACK.click();
    }

    @Step("Нажать на кнопку сохранить")
    public static void saved(){
        SAVED.click();
    }
}
