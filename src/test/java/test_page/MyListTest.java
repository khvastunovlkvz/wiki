package test_page;

import article_page.ArticlePage;
import core_test.CoreTest;
import my_lists_page.MyListsPage;
import navigation_ui.NavigationUI;
import org.testng.annotations.Test;
import search_page.SearchPage;
import welcome_page.WelcomePage;

public class MyListTest extends CoreTest {

    private final String
            SEARCH_TEXT = "Java",
            TEXT_IN_SEARCH_BOX = "Search Wikipedia",
            NAME_ARTICLE_ONE = "Java (programming language)",
            NAME_ARTICLE_TWO = "Javanese language",
            DESCRIPTION_ARTICLE = "Object-oriented programming language",
            MY_NAME_ARTICLE_LIST = "Java";


    @Test(description = "Добавление статьи "+ NAME_ARTICLE_ONE +
            " в новый список " + MY_NAME_ARTICLE_LIST + " и её дальнейшее удаление")
    public void testReusingElements(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.clickArticle(NAME_ARTICLE_ONE);
        //--------------------------//
        ArticlePage.AddAnArticleToANewList(MY_NAME_ARTICLE_LIST);
        //--------------------------//
        NavigationUI.back();
        NavigationUI.back();
        NavigationUI.saved();
        //--------------------------//
        MyListsPage.clickOnAnElement(MY_NAME_ARTICLE_LIST);
        MyListsPage.checkArticle(NAME_ARTICLE_ONE);
        MyListsPage.deletedArticle(NAME_ARTICLE_ONE);
        MyListsPage.checkForMissingArticle(NAME_ARTICLE_ONE);
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Добавление  статей "+ NAME_ARTICLE_ONE +  " и " + NAME_ARTICLE_TWO +
            " в список " + MY_NAME_ARTICLE_LIST + " и удаление " + NAME_ARTICLE_ONE)
    public void testSavedTwoArticlesAndReusingOneArticle(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.clickArticle(NAME_ARTICLE_ONE);
        //--------------------------//
        ArticlePage.AddAnArticleToANewList(MY_NAME_ARTICLE_LIST);
        //--------------------------//
        NavigationUI.back();
        //--------------------------//
        SearchPage.clickArticle(NAME_ARTICLE_TWO);
        //--------------------------//
        ArticlePage.AddAnArticleToMyList(MY_NAME_ARTICLE_LIST);
        //--------------------------//
        NavigationUI.back();
        NavigationUI.back();
        NavigationUI.saved();
        //--------------------------//
        MyListsPage.clickOnAnElement(MY_NAME_ARTICLE_LIST);
        MyListsPage.checkTheNumberOfArticles(2);
        MyListsPage.checkArticle(NAME_ARTICLE_ONE);
        MyListsPage.checkArticle(NAME_ARTICLE_TWO);
        MyListsPage.deletedArticle(NAME_ARTICLE_ONE);
        MyListsPage.checkForMissingArticle(NAME_ARTICLE_ONE);
        MyListsPage.checkThePresenceOfTheArticle(NAME_ARTICLE_TWO);
    }
    //________________________________________________________________________________________________________________//

}
