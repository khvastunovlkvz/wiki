package test_page;

import core_test.CoreTest;
import org.testng.annotations.Test;
import search_page.SearchPage;
import welcome_page.WelcomePage;

public class SearchTest extends CoreTest {

    private final String
            SEARCH_TEXT = "Java",
            TEXT_IN_SEARCH_BOX = "Search Wikipedia",
            SEARCH_NOT_VALIDATE_TEXT = "hghfhfgdhd",
            DESCRIPTION_TEXT = "Object-oriented programming language",
            ARTICLE_1 = "Javanese culture";

    private final int
            NUMBERS_ARTIClES = 8;

    @Test(description = "Проверить есть ли статья с описанием " + DESCRIPTION_TEXT + " в результате поиска")
    public void testSearchJavaOOP(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.visibleResultSearch(DESCRIPTION_TEXT);
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Тест отмены поиска")
    public void testChanelSearchTest(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(" ");
        SearchPage.clickCloseBtnX();
        SearchPage.checkButtonXVisibility();
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Проверить присутствие текста " + TEXT_IN_SEARCH_BOX + " в поисковой строке")
    public void testCheckTextInTheSearchBar(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.checkTextInTheSearchBar(TEXT_IN_SEARCH_BOX);
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Проверить, что результат поиска не нулевой")
    public void checkAmountOfNotEmptySearch() {
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.checkNotEmptySearch();
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Проверить, что результат поиска нулевой")
    public void checkAmountEmptySearch(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_NOT_VALIDATE_TEXT);
        SearchPage.checkTextNoResult();
        SearchPage.checkEmptySearch();
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Проверить есть ли текст " + SEARCH_TEXT + " в найденных статьях")
    public void testCheckWordInArticlesFound(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.checkTextInFoundArticles(SEARCH_TEXT);
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Проверить свайп до статьи " + ARTICLE_1)
    public void testSwipeToArticle(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.checkNotEmptySearch();
        SearchPage.swipeUpToFindElement
                (SearchPage.toArticle(ARTICLE_1));
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Проверить, что количество найденных статей равно " + NUMBERS_ARTIClES)
    public void testCheckTheNumberOfArticlesFound(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.checkNumbersArticles(NUMBERS_ARTIClES);
    }
    //________________________________________________________________________________________________________________//



}
