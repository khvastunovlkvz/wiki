package test_page;

import article_page.ArticlePage;
import core_test.CoreTest;
import org.testng.annotations.Test;
import search_page.SearchPage;
import welcome_page.WelcomePage;

import static java.time.Duration.ofSeconds;

public class ArticleTest extends CoreTest {

    private final String
            SEARCH_TEXT = "Java",
            TEXT_IN_SEARCH_BOX = "Search Wikipedia",
            NAME_ARTICLE_ONE = "Java (programming language)",
            NAME_ARTICLE_TWO = "Javanese language",
            DESCRIPTION_ARTICLE_ONE = "Object-oriented programming language",
            MY_NAME_ARTICLE_LIST = "Java";



    //________________________________________________________________________________________________________________//
    @Test(description = "Проверка описания статьи")
    public void testAssertArticle(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.clickArticle(NAME_ARTICLE_ONE);
        //--------------------------//
        ArticlePage.checkDescriptionArticle(DESCRIPTION_ARTICLE_ONE, 5);
    }
    //________________________________________________________________________________________________________________//

}
