package core_test;
import com.codeborne.selenide.WebDriverRunner;
import driver.MyDriver;
import io.appium.java_client.AppiumDriver;
import listener.MyScreenshotClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.net.MalformedURLException;

@Listeners(MyScreenshotClass.class)
public class CoreTest {

    private static AppiumDriver driver;
    //________________________________________________________________________________________________________________//
    @BeforeMethod
    protected void setUp() throws MalformedURLException {
        driver = MyDriver.setDriver();
        WebDriverRunner.setWebDriver(driver);
    }
    //________________________________________________________________________________________________________________//
    @AfterMethod
    protected void tearDown() {
        WebDriverRunner.closeWebDriver();
        if (driver != null) {
            driver.quit();
        }
    }
    //________________________________________________________________________________________________________________//





}
