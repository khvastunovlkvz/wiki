package test_change__app;

import article_page.ArticlePage;
import core_test.CoreTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import search_page.SearchPage;
import welcome_page.WelcomePage;

import static core_page.CorePage.appInBackground;
import static core_page.CorePage.selectOrientation;
import static org.openqa.selenium.ScreenOrientation.LANDSCAPE;
import static org.openqa.selenium.ScreenOrientation.PORTRAIT;

public class ChangeAppConditionTest extends CoreTest {

    private final String
        SEARCH_TEXT = "Java",
        NAME_ARTICLE_ONE = "Java (programming language)",
        RESULT_SEARCH = "Object-oriented programming language";


    @Test(description = "Проверка описания статьи после ориентации экрана")
    public void checkChangeScreenOrientationOnSearchResults(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.clickArticle(NAME_ARTICLE_ONE);
        //--------------------------//
        String beforeSelectOrientation = ArticlePage.getTextDescriptionArticle();
        //--------------------------//
        selectOrientation(LANDSCAPE);
        //--------------------------//
        String afterSelectOrientation = ArticlePage.getTextDescriptionArticle();
        //--------------------------//
        selectOrientation(PORTRAIT);
        //--------------------------//
        Assert.assertEquals(afterSelectOrientation, beforeSelectOrientation);
    }
    //________________________________________________________________________________________________________________//
    @Test(description = "Проверка результатов поиска после сварачивания приложения")
    public void checkSearchArticleInBackground(){
        WelcomePage.skipWelcomePage();
        //--------------------------//
        SearchPage.initSearchInput();
        SearchPage.setSearchInput(SEARCH_TEXT);
        SearchPage.checkSearchResult(RESULT_SEARCH);
        //--------------------------//
        appInBackground(2);
        //--------------------------//
        SearchPage.checkSearchResult(RESULT_SEARCH);
    }

}
